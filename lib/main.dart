import 'dart:io';

import 'package:ai_test/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:google_generative_ai/google_generative_ai.dart';

Future<void> main() async {
  // final apiKey = dotenv.get('API_KEY');
  // final model = GenerativeModel(model: 'gemini-1.5-flash', apiKey: apiKey);
  //
  // final (firstImage, secondImage) = await (File('image0.jpg').readAsBytes(), File('image1.jpg').readAsBytes()).wait;
  //
  // final prompt = TextPart("What's different between these pictures?");
  //
  // final imageParts = [
  //   DataPart('image/jpeg', firstImage),
  //   DataPart('image/jpeg', secondImage),
  // ];
  //
  // final response = await model.generateContent([
  //   Content.multi([prompt, ...imageParts])
  // ]);
  //
  // print(response.text);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Recipe AI App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
    );
  }
}
